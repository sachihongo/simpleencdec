# SimpleEncDec

Simple Custom Encryption / Decryption of Files using Python

Source Tutorial : https://www.youtube.com/watch?v=xSGnLPTjaXo

How To Encrypt :
- python encrypt.py
- you will see the enc.jpg

How To Decrypt : 
- python decrypt.py
- you will see the result in dec.jog